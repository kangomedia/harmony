/* globals Chart:false, feather:false */

(function () {
  'use strict'

  feather.replace()

  // Graphs
  var ctx = document.getElementById('myChart')
  // eslint-disable-next-line no-unused-vars
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
      ],
      datasets: [{
        label: 'New Associates',
        data: [
          3,
          4,
          7,
          9,
          14,
          24,
          55,
          79,
          99,
          125,
          160,
          190
        ],
        lineTension: 0,
        backgroundColor: 'transparent',
        borderColor: '#FCBD25',
        borderWidth: 4,
        pointBackgroundColor: '#FCBD25'
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: false
          }
        }]
      },
      legend: {
        display: true
      }
    }
  })
}())


var pieOne = document.getElementById('bonusOne');
var chartPieOne = new Chart(pieOne, {
    type: 'doughnut',
    data: {
        labels: ['Earned Months', 'Months to Earn'],
        datasets: [{
            label: '# of Votes',
            data: [25,75],
            backgroundColor: [
                '#5A9B38',
                '#74D348'
            ]
        }]
    },
    options: {
      legend: {
        display: true,
        position: 'bottom',
        fontSize: 10
      },
      // responsive: false
    }
});

var pieTwo = document.getElementById('bonusTwo');
var chartPieTwo = new Chart(pieTwo, {
    type: 'doughnut',
    data: {
        labels: ['Earned Months', 'Months to Earn'],
        datasets: [{
            label: '# of Votes',
            data: [45,55],
            backgroundColor: [
                '#FCBD25',
                '#D19B22'
            ]
        }]
    },
    options: {
      legend: {
        display: true,
        position: 'bottom',
        fontSize: 10
      },
      // responsive: false
    }
});

var pieThree = document.getElementById('bonusThree');
var chartPieThree = new Chart(pieThree, {
    type: 'doughnut',
    data: {
        labels: ['Earned Months', 'Months to Earn'],
        datasets: [{
            label: '# of Votes',
            data: [15,85],
            backgroundColor: [
                '#4CA2DB',
                '#3D8BB7'
            ]
        }]
    },
    options: {
      legend: {
        display: true,
        position: 'bottom',
        fontSize: 10
      },
      // responsive: false
    }
});